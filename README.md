# Conekta API RESTful 
API RESTful para gestionar un buró de crédito

## Spring Framework 4 + Spring Security + OAuth2

## Getting started
### Prerequisites:
- Java 8
- Maven
- Tomcat 8
- MySQL 

## Hosting
- Instance: EC2 
- RDS: MySQL 
- Operating System: Ubuntu 16.04 
- Server: Tomcat 8
- Host: [http://3.16.155.182:8080/ConektaAntiFraudWEB/](http://3.16.155.182:8080/ConektaAntiFraudWEB/)

## Structure of the code ##
    Modules
        - ConektaAntiFraud (MAVEN PARENT)
            - ConektaAntiFraudWEB (CONTROLLER, CONFIG FILES & GlobalException)
            - ConektaAntiFraudDAO (REPOSITORY)
            - ConektaAntiFraudMODEL (MODEL, ENTITY & UTILS)
            - ConektaAntiFraudSERVICE (LOGIC BUSSINES)
        

## Deploy
FTP
Put file ConektaAntiFraudWEB.war into the path: /home/ubuntu

```
$ sudo service tomcat8 stop
& sudo mv ConektaAntiFraudWEB.war /var/lib/tomcat8/webapps/
& sudo service tomcat8 start
```

## Generate packaging (WAR) for deploy into server
```
$ mvn update
$ mvn clean install 
```

## Authentication
* Authentication type :OAUTH2 
```
curl -X POST \
  http://localhost:8080/oauth/token \
  -H 'authorization: Basic c3ByaW5nLXNlY3VyaXR5LW9hdXRoMi1yZWFkLXdyaXRlLWNsaWVudDpzcHJpbmctc2VjdXJpdHktb2F1dGgyLXJlYWQtd3JpdGUtY2xpZW50LXBhc3N3b3JkMTIzNA==' \
  -F grant_type=password \
  -F username=username \
  -F password=passwrod 
  ```
  
## Collection Postman
* [Swagger](http://3.16.155.182:8080/ConektaAntiFraudWEB/swagger-ui.html#/)

## Collection Postman
* [Conekta Exam Collection](https://www.getpostman.com/collections/a2713d4baadfc4a51e32)

## Authors
* **Hugo Jurado** - *Repository* - [ConektaExam](https://bitbucket.org/hugojuradogarcia/conektaexam/src)