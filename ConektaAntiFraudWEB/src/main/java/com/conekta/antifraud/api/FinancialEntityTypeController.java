package com.conekta.antifraud.api;

import com.conekta.antifraud.entity.FinancialEntityType;
import com.conekta.antifraud.model.output.Response;
import com.conekta.antifraud.service.FinancialEntityTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(description = "ADMIN ONLY ACCESS - Save & Update Financial Entity Type")
public class FinancialEntityTypeController {

    @Autowired
    private FinancialEntityTypeService financialEntityTypeService;

    @PostMapping("/admin/saveFinancialEntityTypeAll")
    @ApiOperation(value = "Save Financial Entity Type All")
    public @ResponseBody
    ResponseEntity<Response> saveFinancialEntityTypeAll(@RequestBody @Validated FinancialEntityType request
    ){
        return new ResponseEntity<>(financialEntityTypeService.saveFinancialEntityTypeAll(request), HttpStatus.OK);
    }

    @PostMapping("/admin/saveFinancialEntityType")
    @ApiOperation(value = "Save Financial Entity Type")
    public @ResponseBody ResponseEntity<Response> saveFinancialEntityType(@RequestBody @Validated FinancialEntityType request){
        return new ResponseEntity<>(financialEntityTypeService.saveFinancialEntityType(request), HttpStatus.OK);
    }

    @PutMapping("/admin/updateFinancialEntityType/{id}")
    @ApiOperation(value = "Update Financial Entity Type All")
    public @ResponseBody ResponseEntity<Response> updateFinancialEntityType(@PathVariable @Validated int id, @RequestBody @Validated FinancialEntityType request){
        return new ResponseEntity<>(financialEntityTypeService.updateFinancialEntityType(id, request), HttpStatus.OK);
    }
}
