package com.conekta.antifraud.api;

import com.conekta.antifraud.entity.Cardholder;
import com.conekta.antifraud.model.output.Response;
import com.conekta.antifraud.service.CardholderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(description = "USER ONLY ACCESS - Save, Update & Delete Cardholder")
public class CardholderController {

    @Autowired
    private CardholderService cardholderService;

    @PostMapping("/user/saveCardholder")
    @ApiOperation(value = "Save Cardholder")
    public @ResponseBody
    ResponseEntity<Response> saveCardholder(@RequestBody @Validated Cardholder request){
        return new ResponseEntity<>(cardholderService.saveCardholder(request), HttpStatus.OK);
    }

    @PutMapping("/user/updateCardholder/{id}")
    @ApiOperation(value = "Update Cardholder")
    public @ResponseBody
    ResponseEntity<Response> updateCardholder(@PathVariable @Validated int id, @RequestBody @Validated Cardholder request){
        return new ResponseEntity<>(cardholderService.updateCardholder(id, request), HttpStatus.OK);
    }

    @DeleteMapping("/user/deleteCardholder/{id}")
    @ApiOperation(value = "Delete Cardholder")
    public @ResponseBody
    ResponseEntity<Response> deleteCardholder(@PathVariable @Validated int id){
        return new ResponseEntity<>(cardholderService.deleteCardholder(id), HttpStatus.OK);
    }
}
