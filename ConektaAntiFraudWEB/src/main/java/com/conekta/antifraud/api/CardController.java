package com.conekta.antifraud.api;

import com.conekta.antifraud.entity.Card;
import com.conekta.antifraud.model.output.Response;
import com.conekta.antifraud.service.CardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(description = "USER ONLY ACCESS - Update & Delete Card")
public class CardController {

    @Autowired
    private CardService cardService;

    @PutMapping("/user/updateCard/{id}")
    @ApiOperation(value = "Update Card")
    public @ResponseBody
    ResponseEntity<Response> updateCard(@PathVariable @Validated int id, @RequestBody @Validated Card request){
        return new ResponseEntity<>(cardService.updateCard(id, request), HttpStatus.OK);
    }

    @DeleteMapping("/user/deleteCard/{id}")
    @ApiOperation(value = "Delete Card")
    public @ResponseBody
    ResponseEntity<Response> deleteCard(@PathVariable @Validated int id){
        return new ResponseEntity<>(cardService.deleteCard(id), HttpStatus.OK);
    }
}
