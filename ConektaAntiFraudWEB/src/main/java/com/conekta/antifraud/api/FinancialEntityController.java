package com.conekta.antifraud.api;

import com.conekta.antifraud.entity.FinancialEntity;
import com.conekta.antifraud.model.output.Response;
import com.conekta.antifraud.service.FinancialEntityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(description = "ADMIN ONLY ACCESS - Update & Delete Financial Entity")
public class FinancialEntityController {

    @Autowired
    private FinancialEntityService financialEntityService;

    @PutMapping("/admin/updateFinancialEntity/{id}")
    @ApiOperation(value = "Update Financial Entity")
    public @ResponseBody
    ResponseEntity<Response> updateFinancialEntity(@PathVariable @Validated int id, @RequestBody @Validated FinancialEntity request){
        return new ResponseEntity<>(financialEntityService.updateFinancialEntity(id, request), HttpStatus.OK);
    }

    @DeleteMapping("/admin/deleteFinancialEntity/{id}")
    @ApiOperation(value = "Delete Financial Entity By Id")
    public @ResponseBody
    ResponseEntity<Response> deleteFinancialEntity(@PathVariable @Validated int id){
            return new ResponseEntity<>(financialEntityService.deleteFinancialEntity(id), HttpStatus.OK);
    }
}
