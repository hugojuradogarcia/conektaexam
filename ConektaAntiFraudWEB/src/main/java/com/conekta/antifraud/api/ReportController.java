package com.conekta.antifraud.api;

import com.conekta.antifraud.entity.Report;
import com.conekta.antifraud.model.output.Response;
import com.conekta.antifraud.service.ReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(description = "USER ONLY ACCESS - Find Report")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @PostMapping("/user/saveReport")
    @ApiOperation(value = "Save Report")
    public @ResponseBody
    ResponseEntity<Response> updateCard(@RequestBody @Validated Report request){
        return new ResponseEntity<>(reportService.saveReport(request), HttpStatus.OK);
    }

    @GetMapping("/user/findByRfcAndCardNumberFinal/{rfc}/{cardNumber}")
    @ApiOperation(value = "Find Report By RFC & 4 last digits card number")
    public @ResponseBody
    ResponseEntity<Response> findByRfcAndCardNumberFinal(@PathVariable @Validated String rfc, @PathVariable @Validated int cardNumber){
        return new ResponseEntity<>(reportService.findByRfcAndCardNumberFinal(rfc, cardNumber), HttpStatus.OK);
    }

    @GetMapping("/user/findByRfc/{rfc}")
    @ApiOperation(value = "Find Report By RFC")
    public @ResponseBody
    ResponseEntity<Response> findByRfc(@PathVariable @Validated String rfc){
        return new ResponseEntity<>(reportService.findByRfc(rfc), HttpStatus.OK);
    }

    @GetMapping("/user/findByCardNumberFinal/{cardNumber}")
    @ApiOperation(value = "Find Report 4 last digits card number")
    public @ResponseBody
    ResponseEntity<Response> findByCardNumberFinal(@PathVariable @Validated int cardNumber){
        return new ResponseEntity<>(reportService.findByCardNumberFinal(cardNumber), HttpStatus.OK);
    }
}
