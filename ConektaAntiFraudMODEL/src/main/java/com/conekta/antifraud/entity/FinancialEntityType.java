package com.conekta.antifraud.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "conekta_exam", name = "financial_entity_type")
public class FinancialEntityType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_financial_entity_type", nullable = false)
    private int idFinancialEntityType;

    @Column(name = "financial_entity_type_name")
    private String financialEntityTypeName;

    @OneToMany(mappedBy = "financialEntityType", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<FinancialEntity> financialEntities;

    public void addFinancialEntity(FinancialEntity financialEntity){
        if (financialEntity == null){
            return;
        }
        financialEntity.setFinancialEntityType(this);
        if (financialEntities == null){
            financialEntities = new ArrayList<>();
            financialEntities.add(financialEntity);
        }else if (!financialEntities.contains(financialEntity)){
            financialEntities.add(financialEntity);
        }
    }

    public FinancialEntityType(String financialEntityTypeName) {
        this.financialEntityTypeName = financialEntityTypeName;
    }
}
