package com.conekta.antifraud.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "conekta_exam", name = "cardholder")
@JsonIgnoreProperties( value = {"createdAt", "updatedAt"}, allowGetters = true )
public class Cardholder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_card_holder", nullable = false)
    private int idCardHolder;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "home_phone")
    private String homePhone;

    @Column(name = "mobile_phone")
    private String mobilePhone;

    @Column(name = "rfc", nullable = false)
    private String rfc;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "created_at", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt = new Date();

    @Column(name = "updated_At", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();

    @Column(name = "financial_entity_id")
    private int financialEntityId ;
//    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "financial_entity_id",  insertable = true)
//    private FinancialEntity financialEntity;

    @OneToMany(mappedBy = "cardholder", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Card> cards;

    //@OneToMany(mappedBy = "cardholder", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    //private List<Report> reports;

    public void addCard(Card card){
        if (card == null){
            return;
        }
        card.setCardholder(this);
        if (cards == null){
            cards = new ArrayList<>();
            cards.add(card);
        }else if (!cards.contains(card)){
            cards.add(card);
        }
    }
}
