package com.conekta.antifraud.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "conekta_exam", name = "card")
@JsonIgnoreProperties( value = {"createdAt", "updatedAt"}, allowGetters = true )
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_card", nullable = false)
    private int idCard;

    @Column(name = "amount_credit", nullable = false)
    private String amountCredit;

    @Column(name = "amount_credit_use", nullable = false)
    private String amountCreditUse;

    @Column(name = "created_at", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt = new Date();

    @Column(name = "updated_at", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();

    @Column(name = "card_number", nullable = false)
    private int cardNumber;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "card_holder_id",  insertable = true)
    private Cardholder cardholder;
}
