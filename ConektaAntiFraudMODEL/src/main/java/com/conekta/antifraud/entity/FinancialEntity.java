package com.conekta.antifraud.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "conekta_exam", name = "financial_entity")
@JsonIgnoreProperties( value = {"createdAt", "updatedAt"}, allowGetters = true )
public class FinancialEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_financial_entity", nullable = false)
    private int idFinancialEntity;

    @Column(name = "financial_name", nullable = false)
    private String financialName;

    @Column(name = "enabled")
    private Boolean enabled;

    @Column(name = "created_at", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt = new Date();

    @Column(name = "updated_at", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt = new Date();

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "financial_entity_type_id")
    private FinancialEntityType financialEntityType;

//    @OneToMany(mappedBy = "financialEntity", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    private List<Cardholder> cardholders;
//
//    public void addCardholder(Cardholder cardholder){
//        if (cardholder == null){
//            return;
//        }
//        cardholder.setFinancialEntity(this);
//        if (cardholders == null){
//            cardholders = new ArrayList<>();
//            cardholders.add(cardholder);
//        }else if (!cardholders.contains(cardholder)){
//            cardholders.add(cardholder);
//        }
//    }

    public void setFinancialEntityType(FinancialEntityType financialEntityType) {
        this.financialEntityType = financialEntityType;
    }
}
