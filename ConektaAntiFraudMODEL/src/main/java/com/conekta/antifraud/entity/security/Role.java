package com.conekta.antifraud.entity.security;

import lombok.*;

import javax.persistence.*;


@Data
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "role", uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;
}
