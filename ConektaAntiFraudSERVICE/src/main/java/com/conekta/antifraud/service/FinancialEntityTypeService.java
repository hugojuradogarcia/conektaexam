package com.conekta.antifraud.service;

import com.conekta.antifraud.entity.FinancialEntityType;
import com.conekta.antifraud.model.output.Response;

public interface FinancialEntityTypeService {
    Response saveFinancialEntityTypeAll(FinancialEntityType financialEntityType);
    Response updateFinancialEntityType(int id, FinancialEntityType financialEntityType);
    Response saveFinancialEntityType(FinancialEntityType financialEntityType);
}
