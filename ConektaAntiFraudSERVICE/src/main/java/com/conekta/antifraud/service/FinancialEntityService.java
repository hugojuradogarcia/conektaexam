package com.conekta.antifraud.service;

import com.conekta.antifraud.entity.FinancialEntity;
import com.conekta.antifraud.model.output.Response;

public interface FinancialEntityService {
    Response updateFinancialEntity(int id, FinancialEntity financialEntity);
    Response deleteFinancialEntity(int id);
}
