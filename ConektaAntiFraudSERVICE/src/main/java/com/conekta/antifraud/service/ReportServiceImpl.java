package com.conekta.antifraud.service;

import com.conekta.antifraud.commons.Utils;
import com.conekta.antifraud.dao.CardholderRepository;
import com.conekta.antifraud.dao.CustomReportRepository;
import com.conekta.antifraud.dao.ReportRepository;
import com.conekta.antifraud.entity.Report;
import com.conekta.antifraud.model.output.Response;
import com.conekta.antifraud.model.output.ResponseReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private Utils utils;

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private CustomReportRepository customReportRepository;

    @Autowired
    private CardholderRepository cardholderRepository;

    @Override
    public Response saveReport(Report report) {
        // Validamos si existe la llave foranea a asociar
        if (cardholderRepository.findOne(report.getCardHolderId()) != null) {
            reportRepository.save(report);
            return utils.RESPONSE_OK_SAVE(HttpStatus.OK.toString());
        }
        return utils.RESPONSE_ERROR_ASSOCIATION(HttpStatus.OK.toString());
    }

    @Override
    public Response findByRfcAndCardNumberFinal(String rfc, int carNumberFinal) {
        ResponseReport responseReport = customReportRepository.findByRfcAndCardNumberFinal(rfc, carNumberFinal);

        if (responseReport != null){
            return utils.RESPONSE_FIND_REPORT(HttpStatus.OK.toString(), responseReport);
        }

        return utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString());
    }

    @Override
    public Response findByRfc(String rfc) {
        ResponseReport responseReport = customReportRepository.findByRfc(rfc);

        if (responseReport != null){
            return utils.RESPONSE_FIND_REPORT(HttpStatus.OK.toString(), responseReport);
        }

        return utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString());
    }

    @Override
    public Response findByCardNumberFinal(int carNumberFinal) {
        ResponseReport responseReport = customReportRepository.findByCardNumberFinal(carNumberFinal);

        if (responseReport != null){
            return utils.RESPONSE_FIND_REPORT(HttpStatus.OK.toString(), responseReport);
        }

        return utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString());
    }
}
