package com.conekta.antifraud.service;

import com.conekta.antifraud.entity.Card;
import com.conekta.antifraud.model.output.Response;

public interface CardService {
    Response updateCard(int id, Card card);
    Response deleteCard(int id);
}
