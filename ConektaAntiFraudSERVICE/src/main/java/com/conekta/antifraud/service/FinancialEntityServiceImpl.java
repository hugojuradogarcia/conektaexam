package com.conekta.antifraud.service;

import com.conekta.antifraud.commons.Utils;
import com.conekta.antifraud.dao.FinancialEntityRepository;
import com.conekta.antifraud.entity.FinancialEntity;
import com.conekta.antifraud.model.output.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class FinancialEntityServiceImpl implements FinancialEntityService {

    @Autowired
    private Utils utils;

    @Autowired
    private FinancialEntityRepository financialEntityRepository;

    @Override
    public Response updateFinancialEntity(int id, FinancialEntity financialEntity) {
        FinancialEntity financialEntityExists = financialEntityRepository.findOne(id);

        if (financialEntityExists != null){
            financialEntity.setIdFinancialEntity(id);
            financialEntityRepository.save(financialEntity);
            return utils.RESPONSE_OK_UPDATE(HttpStatus.OK.toString());
        }

        return utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString());
    }

    @Override
    public Response deleteFinancialEntity(int id) {
        FinancialEntity financialEntityExists = financialEntityRepository.findOne(id);

        if (financialEntityExists != null){
            financialEntityRepository.delete(id);
            return utils.RESPONSE_OK_DELETE(HttpStatus.OK.toString());
        }

        return utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString());
    }
}
