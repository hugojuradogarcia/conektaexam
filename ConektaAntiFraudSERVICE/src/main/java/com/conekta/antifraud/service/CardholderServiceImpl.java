package com.conekta.antifraud.service;

import com.conekta.antifraud.commons.Utils;
import com.conekta.antifraud.dao.CardRepository;
import com.conekta.antifraud.dao.CardholderRepository;
import com.conekta.antifraud.dao.FinancialEntityRepository;
import com.conekta.antifraud.entity.Card;
import com.conekta.antifraud.entity.Cardholder;
import com.conekta.antifraud.model.output.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CardholderServiceImpl implements CardholderService {

    @Autowired
    private Utils utils;

    @Autowired
    private CardholderRepository cardholderRepository;

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private FinancialEntityRepository financialEntityRepository;

    @Override
    public Response saveCardholder(Cardholder cardholder) {

        // Validamos si existe la llave foranea a asociar
        if (financialEntityRepository.findOne(cardholder.getFinancialEntityId()) != null) {
            // Validamos si exite un tarjetahabiente con el mismo nombre y rfc
            Cardholder cardHolderExists = cardholderRepository.findFirstByNameAndRfc(cardholder.getName(), cardholder.getRfc());
            List<Card> cardsSave = new ArrayList<>();
            // Validamos si existe el mismo numero de tarjeta
            for (Card card : cardholder.getCards()
            ) {
                if (cardRepository.findFirstByCardNumber(card.getCardNumber()) == null) {
                    cardsSave.add(card);
                }
            }

            if (cardHolderExists == null) {
                cardholder.getCards().clear();
                for (Card card : cardsSave
                ) {
                    cardholder.addCard(card);
                }

                Cardholder cardholderSave = cardholderRepository.save(cardholder);
                return utils.RESPONSE_OK_SAVE(HttpStatus.OK.toString());
            }

            return utils.RESPONSE_EXIST(HttpStatus.OK.toString());
        }
        return utils.RESPONSE_ERROR_ASSOCIATION(HttpStatus.OK.toString());
    }

    @Override
    public Response updateCardholder(int id, Cardholder cardholder) {
        Cardholder cardholderExists = cardholderRepository.findOne(id);

        if (cardholderExists != null){
            cardholder.setIdCardHolder(id);
            cardholderRepository.save(cardholder);
            return utils.RESPONSE_OK_UPDATE(HttpStatus.OK.toString());
        }

        return utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString());
    }

    @Override
    public Response deleteCardholder(int id) {
        Cardholder cardholderExists = cardholderRepository.findOne(id);

        if (cardholderExists != null){
            cardholderRepository.delete(id);
            return utils.RESPONSE_OK_DELETE(HttpStatus.OK.toString());
        }

        return utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString());
    }
}
