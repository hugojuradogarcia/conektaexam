package com.conekta.antifraud.service;

import com.conekta.antifraud.commons.Utils;
import com.conekta.antifraud.dao.CardRepository;
import com.conekta.antifraud.entity.Card;
import com.conekta.antifraud.model.output.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class CardServiceImpl implements CardService {

    @Autowired
    private Utils utils;

    @Autowired
    private CardRepository cardRepository;

    @Override
    public Response updateCard(int id, Card card) {
        Card cardExists = cardRepository.findOne(id);

        if (cardExists != null){
            card.setIdCard(id);
            cardRepository.save(card);
            return utils.RESPONSE_OK_UPDATE(HttpStatus.OK.toString());
        }

        return utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString());
    }

    @Override
    public Response deleteCard(int id) {
        Card cardExists = cardRepository.findOne(id);

        if (cardExists != null){
            cardRepository.delete(id);
            return utils.RESPONSE_OK_DELETE(HttpStatus.OK.toString());
        }

        return utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_FOUND.toString());
    }
}
