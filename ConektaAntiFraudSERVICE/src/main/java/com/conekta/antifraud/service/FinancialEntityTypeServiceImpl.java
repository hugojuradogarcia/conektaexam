package com.conekta.antifraud.service;

import com.conekta.antifraud.commons.Utils;
import com.conekta.antifraud.dao.FinancialEntityRepository;
import com.conekta.antifraud.dao.FinancialEntityTypeRepository;
import com.conekta.antifraud.entity.FinancialEntity;
import com.conekta.antifraud.entity.FinancialEntityType;
import com.conekta.antifraud.model.output.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Service
public class FinancialEntityTypeServiceImpl implements FinancialEntityTypeService {

    @Autowired
    private Utils utils;

    @Autowired
    private FinancialEntityTypeRepository financialEntityTypeRepository;

    @Autowired
    private FinancialEntityRepository financialEntityRepository;

    @Override
    public Response saveFinancialEntityTypeAll(FinancialEntityType financialEntityType) {

        // Validando que el tipo de la entidad financiera exista
        FinancialEntityType financialEntityTypeExist = financialEntityTypeRepository.findByFinancialEntityTypeName(financialEntityType.getFinancialEntityTypeName());
        List<FinancialEntity> financialEntityList = financialEntityType.getFinancialEntities();
        List<FinancialEntity> financialEntitieListSave =  new ArrayList<>(financialEntityList);
//
//        // Set clean don´t repeat items
//        Set<FinancialEntity> s = new LinkedHashSet<>();
//        s.addAll(financialEntityList);
//        s.addAll(financialEntityTypeExist.getFinancialEntities());
//
//        for (FinancialEntity financialEntity: financialEntityList
//             ) {
//            if (financialEntityTypeExist.getFinancialEntities().contains(financialEntity)){
//                financialEntitieListSave.remove(financialEntity);
//            }
//        }

        // Si no hay valores que insertar  en la lista de entidades
        if (financialEntityList != null) {

            // Verificando si la entidad financiera existe, de ser asi es eliminado el row existente
            for (int i = 0; i < financialEntityList.size(); i++) {
                if (financialEntityTypeExist != null){
                    if ((financialEntityRepository.findFirstByFinancialName(financialEntityList.get(i).getFinancialName())) != null &&
                            (financialEntityRepository.findByFinancialNameAndFinancialEntityTypeId(financialEntityList.get(i).getFinancialName(),
                                    financialEntityTypeExist.getIdFinancialEntityType()) != null)){
                        financialEntitieListSave.remove(i);
                    }
                }
                else if (financialEntityRepository.findFirstByFinancialName(financialEntityList.get(i).getFinancialName()) == null) {
                    financialEntitieListSave.remove(i);
                }
            }

            // Si no existe
            if (financialEntityTypeExist == null) {
                FinancialEntityType financialEntityTypePersist = new FinancialEntityType(financialEntityType.getFinancialEntityTypeName());

                for (FinancialEntity financialEntity : financialEntitieListSave
                ) {
                    financialEntityTypePersist.addFinancialEntity(financialEntity);
                }

                financialEntityTypeRepository.save(financialEntityTypePersist);
                return utils.RESPONSE_OK_SAVE(HttpStatus.OK.toString());
            } else {
                financialEntityTypeExist.getFinancialEntities().clear();
                for (FinancialEntity financialEntity : financialEntitieListSave
                ) {
                    financialEntityTypeExist.addFinancialEntity(financialEntity);
                }

                financialEntityTypeRepository.save(financialEntityTypeExist);
                return utils.RESPONSE_OK_SAVE(HttpStatus.OK.toString());
            }
        }

        return utils.RESPONSE_EXIST(HttpStatus.NOT_MODIFIED.toString());
    }

    @Override
    public Response updateFinancialEntityType(int id, FinancialEntityType financialEntityType) {

        FinancialEntityType financialEntityTypeExists = financialEntityTypeRepository.findOne(id);

        if (financialEntityTypeExists != null){
            financialEntityType.setIdFinancialEntityType(id);
            financialEntityTypeRepository.save(financialEntityType);
            return utils.RESPONSE_OK_UPDATE(HttpStatus.OK.toString());
        }

        return utils.RESPONSE_NOT_FOUND(HttpStatus.NOT_MODIFIED.toString());
    }

    @Override
    public Response saveFinancialEntityType(FinancialEntityType financialEntityType) {

        FinancialEntityType financialEntityTypeExists = financialEntityTypeRepository.findByFinancialEntityTypeName(financialEntityType.getFinancialEntityTypeName());

        if (financialEntityTypeExists == null){
            financialEntityTypeRepository.save(financialEntityType);
            return utils.RESPONSE_OK_SAVE(HttpStatus.OK.toString());
        }

        return utils.RESPONSE_EXIST(HttpStatus.NOT_MODIFIED.toString());
    }
}
