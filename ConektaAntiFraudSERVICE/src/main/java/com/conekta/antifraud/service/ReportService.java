package com.conekta.antifraud.service;

import com.conekta.antifraud.entity.Report;
import com.conekta.antifraud.model.output.Response;

public interface ReportService {
    Response saveReport(Report report);
    Response findByRfcAndCardNumberFinal(String rfc, int carNumberFinal);
    Response findByRfc(String rfc);
    Response findByCardNumberFinal(int carNumberFinal);
}
