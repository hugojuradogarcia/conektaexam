package com.conekta.antifraud.service;

import com.conekta.antifraud.entity.Cardholder;
import com.conekta.antifraud.model.output.Response;

public interface CardholderService {
    Response saveCardholder(Cardholder cardholder);
    Response updateCardholder(int id, Cardholder cardholder);
    Response deleteCardholder(int id);
}
