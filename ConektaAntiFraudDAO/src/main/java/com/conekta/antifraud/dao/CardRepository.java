package com.conekta.antifraud.dao;

import com.conekta.antifraud.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends JpaRepository<Card, Integer> {
    Card findFirstByCardNumber(int cardNumber);
}
