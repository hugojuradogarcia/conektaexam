package com.conekta.antifraud.dao;

import com.conekta.antifraud.model.output.ResponseReport;

public interface CustomReportRepository {
    ResponseReport findByRfcAndCardNumberFinal(String rfc, int cardNumberFinal);
    ResponseReport findByRfc(String rfc);
    ResponseReport findByCardNumberFinal(int cardNumberFinal);
}
