package com.conekta.antifraud.dao;

import com.conekta.antifraud.entity.Cardholder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardholderRepository extends JpaRepository<Cardholder, Integer> {
    Cardholder findFirstByNameAndRfc(String name, String rfc);
}
